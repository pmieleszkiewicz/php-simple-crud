<?php
require_once 'vendor/autoload.php';

use Application\Validation as Validation;
use Application\DatabaseHandler as DatabaseHandler;

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);
$db = new DatabaseHandler();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $email = $_POST['email'];
    $dateOfBirth = $_POST['dateOfBirth'];


    $addingResult = false; // query result: success / fail
    $error; // holds type of error

    if (Validation::validateNewUser($name, $surname, $email, $dateOfBirth, $error)) {
        $name = htmlspecialchars(trim($name));
        $surname = htmlspecialchars(trim($surname));
        $email = htmlspecialchars(trim($email));
        $dateOfBirth = htmlspecialchars(trim($dateOfBirth));

        $db->addUser($name, $surname, $email, $dateOfBirth, $addingResult);
    }

    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $rows = $db->getResult($query);
    echo $twig->render('index.html', ['rows' => $rows, 'result' => $addingResult, 'resultMsg' => 'The user has been added!', 'error' => $error]);
} else{
    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $rows = $db->getResult($query);
    echo $twig->render('index.html', ['rows' => $rows, 'result' => false, 'resultMsg' => '', 'error' => 'You must fill all fields!']);
}
