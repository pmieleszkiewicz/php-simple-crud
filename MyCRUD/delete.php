<?php
require_once 'vendor/autoload.php';

use Application\Validation as Validation;
use Application\DatabaseHandler as DatabaseHandler;

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);
$db = new DatabaseHandler();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!isset($_GET['id'])) {
        $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
        $rows = $db->getResult($query);
        echo $twig->render('index.html', ['rows' => $rows, 'result' => false, 'resultMsg' => '', 'error' => 'The user does not exist!']);
        exit();
    }
    $id = $_GET['id'];

    $db->deleteUser($id, $queryResult);

    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $result = $db->getResult($query);

    if ($queryResult == 1) {
        echo $twig->render('index.html', ['rows' => $result, 'resultMsg' => 'The user was deleted successfully!', 'result' => $queryResult]);
    } else {
        echo $twig->render('index.html', ['rows' => $result, 'resultMsg' => 'The user was not deleted successfully!', 'result' => $queryResult]);
    }
}
