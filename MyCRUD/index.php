<?php
require_once 'vendor/autoload.php';

use Application\DatabaseHandler as DatabaseHandler;

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$db = new DatabaseHandler();
$query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
$result = $db->getResult($query);
echo $twig->render('index.html', ['rows' => $result]);