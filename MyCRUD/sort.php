<?php
require_once 'vendor/autoload.php';

use Application\Validation as Validation;
use Application\DatabaseHandler as DatabaseHandler;

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);
$db = new DatabaseHandler();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!isset($_GET['by'])) {
        $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
        $rows = $db->getResult($query);
        echo $twig->render('index.html', ['rows' => $rows, 'result' => false, 'resultMsg' => '', 'error' => 'Invalid sorting option!']);
        exit();
    }

    $sortBy = $_GET['by'];

    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $rows = $db->sortedRows($sortBy);
    $sort = ucfirst($sortBy);
    switch ($sortBy) {
        case 'name':
            echo $twig->render('index.html', ['rows' => $rows, 'result' => true, 'resultMsg' => "Sorted by: $sort", 'error' => 'Something went wrong!']);
            break;
        case 'surname':
            echo $twig->render('index.html', ['rows' => $rows, 'result' => true, 'resultMsg' => "Sorted by: $sort", 'error' => 'Something went wrong!']);
            break;
        case 'email':
            echo $twig->render('index.html', ['rows' => $rows, 'result' => true, 'resultMsg' => "Sorted by: $sort", 'error' => 'Something went wrong!']);
            break;
        case 'dateOfBirth':
            echo $twig->render('index.html', ['rows' => $rows, 'result' => true, 'resultMsg' => "Sorted by: $sort", 'error' => 'Something went wrong!']);
            break;
        default:
            $rows = $db->getResult($query);
            echo $twig->render('index.html', ['rows' => $rows, 'result' => false, 'resultMsg' => 'The user has been added!', 'error' => 'Invalid sorting option!']);
    }
}