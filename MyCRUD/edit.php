<?php
require_once 'vendor/autoload.php';

use Application\Validation as Validation;
use Application\DatabaseHandler as DatabaseHandler;

# Puts inputs instead of user info
function formatRows(&$rows, $id) {
    foreach ($rows as &$row) {
        if ($row['id'] == $id) {
            $row['name'] = '<input type="text" name="name" value="' . $row['name'] . '" />';
            $row['surname'] = '<input type="text" name="surname" value="' . $row['surname'] . '" />';
            $row['email'] = '<input type="text" name="email" value="' . $row['email'] . '" />';
            $row['dateOfBirth'] = '<input type="text" name="dateOfBirth" value="' . $row['dateOfBirth'] . '" />';
        }
    }
}

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    $db = new DatabaseHandler();
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    } else {
        $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
        $rows = $db->getResult($query);
        echo $twig->render('index.html', ['rows' => $rows, 'result' => false, 'resultMsg' => 'The user does not exist!', 'error' => 'The user does not exist!']);
        exit();
    }

    $isUserOk = $db->userExist($id);

    # table rows
    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $rows = $db->getResult($query);
    if ($isUserOk) {
        # modify $user - put inputs instead of values
        formatRows($rows, $id, $isUserOk);
        echo $twig->render('index.html', ['rows' => $rows, 'resultMsg' => 'You can modify user data now.',
            'result' => $isUserOk, 'editMode' => true, 'idToEdit' => $id]);
    } else {
        echo $twig->render('index.html', ['rows' => $rows, 'error' => 'User does not exist!', 'result' => $isUserOk]);
    }
}
else {
    # update user data
    $db = new DatabaseHandler();
    #var_dump($_POST);
    $id = $_POST['id'];
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $email = $_POST['email'];
    $oldEmail = $db->getEmail($id);
    $dateOfBirth = $_POST['dateOfBirth'];
    $editResult = false;

    if (Validation::validateEditedUser($id, $name, $surname, $email, $dateOfBirth, $error)) {
        $name = htmlspecialchars($name);
        $surname = htmlspecialchars($surname);
        $email = htmlspecialchars($email);
        $dateOfBirth = htmlspecialchars($dateOfBirth);
        $db->updateUser($id, $name, $surname, $email, $dateOfBirth, $editResult);
    }

    $query = 'SELECT id, name, surname, email, dateOfBirth, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age FROM users';
    $rows = $db->getResult($query);
    echo $twig->render('index.html', ['rows' => $rows, 'result' => $editResult, 'resultMsg' => 'The user has been modified!', 'error' => $error]);
}
