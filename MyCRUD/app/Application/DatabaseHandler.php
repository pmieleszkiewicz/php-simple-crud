<?php


namespace Application;


class DatabaseHandler
{
    private $_host = 'localhost';
    private $_username = 'user'; #id2842664_hipolit
    private $_password = '2bIBs4NrvCTsMuYd'; #2bIBs4NrvCTsMuYd
    private $_database = 'crud'; #crud

    private $connection;

    public function __construct() {
        if (!isset($connection)) {
            try {
                $this->connection = new \PDO("mysql:host={$this->_host};dbname={$this->_database};charset=utf8", $this->_username, $this->_password);
            }
            catch (\PDOException $e) {
                $this->connection = null;
                die('<h2>Unable to connect to the database!</h2>');
            }
        }
    }

    // used for const queries ( without variables )
    public function getResult($query) {
        $statement = $this->connection->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function addUser($name, $surname, $email, $dateOfBirth, &$queryResult) {
        $statement = $this->connection->prepare("INSERT INTO users(name, surname, email, dateOfBirth) VALUES (:name, :surname, :email, :dateOfBirth)");
        $statement->bindValue(':name', $name, \PDO::PARAM_STR);
        $statement->bindValue(':surname', $surname, \PDO::PARAM_STR);
        $statement->bindValue(':email', $email, \PDO::PARAM_STR);
        $statement->bindValue(':dateOfBirth', $dateOfBirth, \PDO::PARAM_STR);
        $queryResult = $statement->execute();
    }

    public function updateUser($id, $name, $surname, $email, $dateOfBirth, &$queryResult) {
        $statement = $this->connection->prepare("UPDATE users SET name=:name, surname=:surname, email=:email, dateOfBirth=:dateOfBirth WHERE id=:id");
        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
        $statement->bindValue(':name', $name, \PDO::PARAM_STR);
        $statement->bindValue(':surname', $surname, \PDO::PARAM_STR);
        $statement->bindValue(':email', $email, \PDO::PARAM_STR);
        $statement->bindValue(':dateOfBirth', $dateOfBirth, \PDO::PARAM_STR);
        $statement->execute();
        $queryResult = (count($statement->errorInfo() == 0)) ? true : false;
    }

    public function deleteUser($id, &$queryResult) {
        $statement = $this->connection->prepare("DELETE FROM users WHERE id=:id");
        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
        $queryResult = $statement->execute();
    }

//    public function getUser($id) {
//        echo $id;
//        $statement = $this->connection->prepare("SELECT * FROM users WHERE id=:id");
//        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
//        $statement->execute();
//        $row = $statement->fetch();
//        return $row;
//    }

    public function editUser($id, &$queryResult) {
        $statement = $this->connection->prepare("DELETE FROM users WHERE id=:id");
        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
        $queryResult = $statement->execute();
    }

    public function userExist($id) {
        $statement = $this->connection->prepare("SELECT id FROM users WHERE id=:id");
        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->rowCount();
        return $result;
    }

    public function isEmailAvailable($email) {
        $statement = $this->connection->prepare("SELECT email FROM users WHERE email=:email");
        $statement->bindValue(':email', $email, \PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->rowCount();
        return !$result;
    }

    public function getEmail($id){
        $statement = $this->connection->prepare("SELECT email FROM users WHERE id=:id");
        $statement->bindValue(':id', $id, \PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result[0]['email'];
    }

    public function getId($email) {
        $statement = $this->connection->prepare("SELECT id FROM users WHERE email=:email");
        $statement->bindValue(':email', $email, \PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        if (!empty($result)){
            return $result[0]['id'];
        } else {
            return false;
        }
    }

    public function sortedRows($sortBy) {
        $statement = $this->connection->prepare("SELECT id, name, surname, email, TIMESTAMPDIFF(YEAR, dateOfBirth, sysdate()) as age, dateOfBirth FROM users ORDER BY $sortBy");
        $statement->execute();
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public function __toString()
    {
        return "{$this->_database}";
    }
}