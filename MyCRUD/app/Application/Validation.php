<?php


namespace Application;

use Application\DatabaseHandler as DatabaseHandler;

class Validation
{
    public static function validateNewUser($name, $surname, $email, $date, &$error) {
        if (empty($name) || empty($surname) || empty($email) || empty($date)) {
            $error = 'You must fill all fields!';
            return false;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Invalid e-mail!';
            return false;
        }
        $db = new DatabaseHandler();

        if (!$db->isEmailAvailable($email)) {
            $error = 'E-mail is in use!';
            return false;
        }

        #date validation
        $dateResult = \DateTime::createFromFormat('Y-m-d', $date);
        $date_errors = \DateTime::getLastErrors();
        if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
            $error = 'Invalid date format! [ YYYY/MM/DD ]';
            return false;
        }
        return true;
    }


    public static function validateEditedUser($id, $name, $surname, $email, $date, &$error) {
        if (empty($name) || empty($surname) || empty($email) || empty($date)) {
            $error = 'You must fill all fields!';
            return false;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = 'Invalid e-mail!';
            return false;
        }
        $db = new DatabaseHandler();

        if ($id != $db->getId($email)) {
            if (!$db->isEmailAvailable($email)) {
                $error = 'E-mail is in use!';
                return false;
            }
        }

        #date validation
        $dateResult = \DateTime::createFromFormat('Y-m-d', $date);
        $date_errors = \DateTime::getLastErrors();
        if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
            $error = 'Invalid date format! [ YYYY/MM/DD ]';
            return false;
        }
        return true;
    }
}