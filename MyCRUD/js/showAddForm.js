let isNone = true;

let addButton = document.getElementById('addButton');
addButton.addEventListener('click', function() {
    let buttonBox = document.getElementById('buttons');
    if (isNone) {
        buttonBox.style.display = "block";
        isNone = false;
    } else {
        buttonBox.style.display = "none";
        isNone = true;
    }
}, false);